#include "image.h"
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    struct image new_image = {
            width,
            height,
            malloc(width * height * sizeof(struct pixel))};
    return new_image;

}

void destroy_image(struct image *image){
    free(image -> data);
    image -> data = NULL;
}
