#include "image.h"
#include "opening_and_closing_file.h"
#include "rotating.h"
#include "serializer_and_deserializer.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        printf("incorrect count of arguments");
        return 1;
    }

    struct image my_image = {0};
    FILE* in = NULL;
    FILE* out = NULL;

    const char* original_image_directory = argv[1];
    const char* rotated_image_directory = argv[2];

    // opening a file
    enum status_of_opening status_of_opening_file = open_file_for_reading(&in, original_image_directory);
    if (status_of_opening_file != 0) return status_of_opening_file;


    // deserializing
    enum read_status status_of_deserializing = from_bmp(in, &my_image);
    if (status_of_deserializing != 0) return status_of_deserializing;

    // closing a file
    enum status_of_closing status_of_closing_file = close_file(in);
    if (status_of_closing_file != 0) return status_of_closing_file;

    // rotation
    struct image rotated_image = rotate(&my_image);

    //Open transformed image file for writing
    enum status_of_opening status_of_opening_file_for_writing = open_file_for_writing(&out, rotated_image_directory);
    if  (status_of_opening_file_for_writing != 0 )  return status_of_opening_file_for_writing;

    // serializing
    enum write_status status_of_serializing = to_bmp(out, &rotated_image);
    if (status_of_serializing != 0 )  return status_of_serializing;

    // closing a file
    enum status_of_closing status_of_final_closing  = close_file(out);
    if  (status_of_final_closing != 0)  return status_of_final_closing;

    //free
    destroy_image(&my_image);
    destroy_image(&rotated_image);
    return 0;

}
