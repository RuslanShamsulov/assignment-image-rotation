#include "rotating.h"
#include "image.h"
#include <stdlib.h>


struct image rotate( struct image const* original ) {
    struct image rotated = create_image(original->height, original->width);  // create_image(uint64_t width, uint64_t height)   (меняем местами)
    uint64_t Height = original->width;
    uint64_t Width = original->height;
    for (uint64_t  i = 0; i < Height; i++) {
        for (uint64_t  j = 0; j < Width; j++) {
            rotated.data[Width * i + j] = original->data[(Width-j-1)*Height + i];
        }
    }
    return rotated;
}

