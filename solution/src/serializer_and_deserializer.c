#include "serializer_and_deserializer.h"
#include <stdint.h>
#include <stdio.h>


struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;            // 0x4D42
    uint32_t  bfileSize;        // размер файла в байтах
    uint32_t bfReserved;        // зарезервированные данные, нули
    uint32_t bOffBits;          // где начинается сам битовый массив относительно начала файла
    uint32_t biSize;            // размер самой структуры
    uint32_t biWidth;           // ширина в пикселях
    uint32_t  biHeight;         // высота в пикселях
    uint16_t  biPlanes;         // количество плоскостей. Пока оно всегда устанавливается в 1.
    uint16_t biBitCount;        // Количество бит на один пиксель
    uint32_t biCompression;     // тип сжатия, обычно bmp без сжатия
    uint32_t biSizeImage;       // размер картинки в байтах
    uint32_t biXPelsPerMeter;   // горизонтальное разрешение (пикс/метр), куда выводится битовый массив
    uint32_t biYPelsPerMeter;   // вертикальное разрешение (пикс/метр), куда выводится битовый массив
    uint32_t biClrUsed;         // кол-во цветов (если ноль, то макс кол-во цветов)
    uint32_t  biClrImportant;   // число цветов, которые необходимы для того, чтобы изобразить рисунок
};

static uint8_t get_padding(struct image const* img) {
    return (img -> width  % 4);
}

/* вынесенная функция для уменьшения функции from_bmp*/
static enum read_status check_parameters_of_header (const struct bmp_header header) {

    if (header.biBitCount != 24)      return READ_INVALID_BITS;
    if (header.bfReserved != 0)       return READ_INVALID_SIGNATURE;
    if (header.bfType != 0x4D42)      return READ_INVALID_SIGNATURE;     // должно быть little indian
    if (header.biPlanes != 1)         return READ_INVALID_PLANES;
    if (header.biCompression != 0)    return READ_INVALID_TYPE_OF_COMPRESSION;
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header  header;

   if (fread( &header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
   if (in == NULL) return READ_INVALID_SOURCE;

    enum read_status header_status = check_parameters_of_header(header);
    if (header_status != 0) return header_status;

    *img = create_image(header.biWidth, header.biHeight);
    if (img -> data == NULL)  return READ_INVALID_IMAGE_MEMORY;

    const uint8_t  padding = get_padding(img);
    for (size_t row = 0; row < img->height; row++){
        if (fread(&(img->data[row*img -> width]), sizeof(struct pixel), img -> width, in) != img->width) return READ_INVALID_PIXELS; // считываем пачками пиксели по строкам
        if (fseek(in, padding, SEEK_CUR)!= 0) return READ_INVALID_PADDING; // пропускаем байты паддинговые
    }
    return READ_OK;
}



static struct bmp_header creating_header(struct image img) {

    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (img.width) * (img.height) * sizeof(struct pixel) + (img.height) * (img.width % 4) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  sizeof(struct pixel)*img.height * img.width + (img.width % 4)*(img.height),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return header;
}





enum write_status to_bmp( FILE* out, struct image* img ) {
    const size_t to_fill_empty_space = 0;
    struct bmp_header header = creating_header(*img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)  return WRITE_HEADER_ERROR;

    uint8_t padding = get_padding(img);
    for (size_t row = 0; row < img->height; row++) {
        if (fwrite(&(img->data[row*img -> width]), sizeof(struct pixel), img -> width, out) != img -> width) return WRITE_ERROR_WITH_PIXELS;
        if (fwrite(&to_fill_empty_space, 1, padding, out) != padding) return WRITE_ERROR_WITH_PADDING;
    }
    return WRITE_OK;
}
