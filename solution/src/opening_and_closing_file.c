#include "opening_and_closing_file.h"
#include <stdio.h>

enum status_of_opening open_file_for_reading(FILE** file, const char* filepath) {
    *file = fopen(filepath, "r");
    if (*file == NULL) return OPENING_ERROR; else return OPENING_SUCCESSFUL;
}

enum status_of_opening open_file_for_writing(FILE** file, const char* filepath) {
    *file = fopen(filepath,"wb");
    if (*file == NULL) return OPENING_ERROR; else return OPENING_SUCCESSFUL;
}

enum status_of_closing close_file(FILE* file) {
    if (file == NULL) return CLOSING_ERROR;
    fclose(file);
    return CLOSING_SUCCESSFUL;
}
