#ifndef SERIALIZER_AND_DESERIALIZER_H
#define SERIALIZER_AND_DESERIALIZER_H
#include "image.h"
#include <stdio.h>


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PLANES,
    READ_INVALID_TYPE_OF_COMPRESSION,
    READ_INVALID_IMAGE_MEMORY,
    READ_INVALID_SOURCE,
    READ_INVALID_PIXELS,
    READ_INVALID_PADDING
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_ERROR_WITH_PADDING,
    WRITE_ERROR_WITH_PIXELS
};


/* deserializer */
enum read_status from_bmp(FILE * in, struct image *img);


/* serializer */
enum write_status to_bmp(FILE * out, struct image *img);


#endif
