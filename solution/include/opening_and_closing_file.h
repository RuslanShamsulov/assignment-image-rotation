#ifndef OPENING_AND_CLOSING_H
#define OPENING_AND_CLOSING_H
#include <stdio.h>


enum status_of_opening {
    OPENING_SUCCESSFUL = 0,
    OPENING_ERROR
};

enum status_of_closing {
    CLOSING_SUCCESSFUL = 0,
    CLOSING_ERROR
};

enum status_of_opening open_file_for_reading(FILE** file, const char *filepath);
enum status_of_opening open_file_for_writing(FILE** file, const char *filepath);
enum status_of_closing close_file(FILE *file);

#endif
