#ifndef ROTATING_H
#define ROTATING_H
#include "image.h"


struct image rotate(struct image const* original);

#endif
